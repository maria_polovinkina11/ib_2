﻿
// DESDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "DES.h"
#include "DESDlg.h"
#include "afxdialogex.h"
#include <fstream> 
#include <iostream> 

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;
// Диалоговое окно CDESDlg



CDESDlg::CDESDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DES_DIALOG, pParent)
	, Text(_T("Hello new world!"))
	, Encr_txt(_T(""))
	, Decr_txt(_T(""))
	, Key(_T("1245dffd"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDESDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ishod_txt, Text);
	DDX_Text(pDX, IDC__Encrypt_txt, Encr_txt);
	DDX_Text(pDX, IDC_decrypt_txt, Decr_txt);
	DDX_Text(pDX, IDC_key, Key);
}

BEGIN_MESSAGE_MAP(CDESDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_Start, &CDESDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_Encrypt, &CDESDlg::OnBnClickedButtonEncrypt)
	ON_BN_CLICKED(IDC_BUTTON_Decrypt, &CDESDlg::OnBnClickedButtonDecrypt)
	ON_BN_CLICKED(IDC_BUTTON_GenKey, &CDESDlg::OnBnClickedButtonGenkey)
	ON_BN_CLICKED(IDC_BUTTON_Load, &CDESDlg::OnBnClickedButtonLoad)
END_MESSAGE_MAP()


// Обработчики сообщений CDESDlg

BOOL CDESDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CDESDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CDESDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Generate(vector<unsigned char>& key)//рандомная генерация ключа
{
	srand(time(0));
	key.clear();
	for (int i = 0; i < 8; i++)
	{
		key.push_back(rand() % 94 + 32);
	}
}


void CDESDlg::OnBnClickedButtonGenkey()//Сгенерировать ключ
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);
	vector<unsigned char> _key;
	Generate(_key);
	string _key_;
	for (int i = 0; i < _key.size(); i++)
	{
		_key_ += _key[i];
	}
	Key = _key_.c_str();
	UpdateData(FALSE);
}
string Code_1;
string Code_2;




void CDESDlg::txt_to_bool()
{
	int length = Text.GetLength();

	if (length % 8 != 0)
	{
		for (int i = 0; i < (8 - length % 8); i++)
			Text += '\0';
	}

	int* buf = new int[Text.GetLength()];
	for (int i = 0; i < Text.GetLength(); i++)
	{
		buf[i] = Text.GetAt(i);
	}

	bool* buf_bool = new bool[Text.GetLength() * 8];
	text.clear();
	text.resize(Text.GetLength() * 8);

	int del;
	for (int i = 0; i < Text.GetLength(); i++)
	{
		del = buf[i];
		for (int j = 0; j < 8; j++)
		{
			buf_bool[i * 8 + j] = del % 2;
			del = del / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			text[i * 8 + j] = buf_bool[i * 8 + 7 - j];
		}
	}
	delete[] buf;
	delete[] buf_bool;

}

void CDESDlg::str_encrypt_to_bool()
{
	int razmer = Encr_txt.GetLength();

	if (razmer % 8 != 0)
	{
		for (int i = 0; i < (8 - razmer % 8); i++)
			Encr_txt += '\0';
	}

	int* buffer = new int[Encr_txt.GetLength()];
	for (int i = 0; i < Encr_txt.GetLength(); i++)
	{
		buffer[i] = Encr_txt.GetAt(i);
	}

	bool* buffer_bool = new bool[Encr_txt.GetLength() * 8];
	shifr_text.clear();
	shifr_text.resize(Encr_txt.GetLength() * 8);


	int delimoe;
	for (int i = 0; i < Encr_txt.GetLength(); i++)
	{
		delimoe = buffer[i];
		for (int j = 0; j < 8; j++)
		{
			buffer_bool[i * 8 + j] = delimoe % 2;
			delimoe = delimoe / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			shifr_text[i * 8 + j] = buffer_bool[i * 8 + 7 - j];
		}
	}
	delete[] buffer;
	delete[] buffer_bool;

}

void CDESDlg::key_to_bool()
{
	int* buffer = new int[8];
	for (int i = 0; i < 8; i++)
	{
		buffer[i] = Key.GetAt(i);
	}

	bool* buffer_bool = new bool[64];
	Key_.clear();
	Key_.resize(64);

	int delimoe;
	for (int i = 0; i < 8; i++)
	{
		delimoe = buffer[i];
		for (int j = 0; j < 8; j++)
		{
			buffer_bool[i * 8 + j] = delimoe % 2;
			delimoe = delimoe / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			Key_[i * 8 + j] = buffer_bool[i * 8 + 7 - j];
		}
	}

	delete[] buffer;
	delete[] buffer_bool;
}

void CDESDlg::OnBnClickedButtonLoad()//Загрузить текст
{
	// TODO: добавьте свой код обработчика уведомлений
	Text = _T("");
	CString Name;// буффер имени файла
	TCHAR szFilters[] = _T("Текстовые документы (*.txt)|*.txt|Все файлы (*.*)|*.*||");// паттерн поиска файла по расширениям
	CFileDialog fLoadTextDlg(TRUE, _T("txt"), _T("*.txt"), OFN_FILEMUSTEXIST || OFN_HIDEREADONLY, szFilters);
	CString strLine;// все три строчки вниз - буфферы для чтения файла
	if (fLoadTextDlg.DoModal() == IDOK) {//далее цикл чтения
		Name = fLoadTextDlg.GetPathName();
		CStdioFile fLoadText;
		if (fLoadText.Open((LPCTSTR)Name, CFile::modeRead | CFile::typeText)) {
			while (true) {
				if (!fLoadText.ReadString(strLine) && strLine.IsEmpty()) break;
				else Text += strLine + _T("\r\n");
			}
			fLoadText.Close();
		}
		else
		{
			MessageBox((LPCTSTR)_T("\r\nФайл не открывается"), (LPCTSTR)_T("Ошибка"));
			return;
			fLoadText.Close();
		}
	}
	else MessageBox((LPCTSTR)_T("\r\nФайл не открывается"), (LPCTSTR)_T("Ошибка"));
	UpdateData(FALSE);
	Code_1 = " ";
	for (int i = 0; i < Text.GetLength(); i++)
	{
		Code_1 += Text[i];
	}
}

void CDESDlg::OnBnClickedButtonStart()//Выполнить
{
	// TODO: добавьте свой код обработчика уведомлений

}


void CDESDlg::OnBnClickedButtonEncrypt()//Зашифровать
{
	// TODO: добавьте свой код обработчика уведомлений

	Encr_txt.Empty();


	const int Sj[8][64] = {
	{ 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
	0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
	4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
	15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 },

	{ 15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
	3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
	0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
	13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 },

	{ 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
	13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
	13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
	1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 },

	{ 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
	13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
	10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
	3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 },

	{ 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
	14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
	4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
	11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 },

	{ 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
	10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
	9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 1, 6,
	4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 1 },

	{ 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
	13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
	1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
	6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 },

	{ 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
	1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
	7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
	2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 }
	};

	const int P[] =
	{
		16, 7, 20, 21, 29, 12, 28, 17,
		1, 15, 23, 26, 5, 18, 31, 10,
		2, 8, 24, 14, 32, 27, 3, 9,
		19, 13, 30, 6, 22, 11, 4, 25
	};

	const int IP[] = { 58, 50, 42, 34, 26, 18, 10, 2,
		60, 52, 44, 36, 28, 20, 12, 4,
		62, 54, 46, 38, 30, 22, 14, 6,
		64, 56, 48, 40, 32, 24, 16, 8,
		57, 49, 41, 33, 25, 17, 9, 1,
		59, 51, 43, 35, 27, 19, 11, 3,
		61, 53, 45, 37, 29, 21, 13, 5,
		63, 55, 47, 39, 31, 23, 15, 7 };

	const int IP_1[] = { 40, 8, 48, 16, 56, 24, 64, 32,
		39, 7, 47, 15, 55, 23, 63, 31,
		38, 6, 46, 14, 54, 22, 62, 30,
		37, 5, 45, 13, 53, 21, 61, 29,
		36, 4, 44, 12, 52, 20, 60, 28,
		35, 3, 43, 11, 51, 19, 59, 27,
		34, 2, 42, 10, 50, 18, 58, 26,
		33, 1, 41, 9, 49, 17, 57, 25 };

	const int IP_for_key[] = { 57, 49, 41, 33, 25, 17, 9, 1,
		58, 50, 42, 34, 26, 18, 10, 2,
		59, 51, 43, 35, 27, 19, 11, 3,
		60, 52, 44, 36, 63, 55, 47, 39,
		31, 23, 15, 7, 62, 54, 46, 38,
		30, 22, 14, 6, 61, 53, 45, 37,
		29, 21, 13, 5, 28, 20, 12, 4 };

	const int roker_key[] = { 14, 17, 11, 24, 1, 5,
		3, 28, 15, 6, 21, 10,
		23, 19, 12, 4, 26, 8,
		16, 7, 27, 20, 13, 2,
		41, 52, 31, 37, 47, 55,
		30, 40, 51, 45, 33, 48,
		44, 49, 39, 56, 34, 53,
		46, 42, 50, 36, 29, 32 };

	static int expansion[] = { 32, 1, 2, 3, 4, 5, 4, 5,
		6, 7, 8, 9, 8, 9, 10, 11,
		12, 13, 12, 13, 14, 15, 16, 17,
		16, 17, 18, 19, 20, 21, 20, 21,
		22, 23, 24, 25, 24, 25, 26, 27,
		28, 29, 28, 29, 30, 31, 32, 1 };

	txt_to_bool();


	bool* buf = new bool[Text.GetLength() * 8];
	int num_at_block, num_at_block_with_IP;

	for (int i = 0; i < Text.GetLength() / 8; i++)
	{
		for (int j = 0; j < 64; j++)
		{
			num_at_block = i * 64 + j;
			num_at_block_with_IP = i * 64 + IP[j] - 1;

			buf[num_at_block] = text[num_at_block_with_IP];
		}
	}

	for (int i = 0; i < Text.GetLength() * 8; i++)
	{
		text[i] = buf[i];
	}

	delete[] buf;


	key_to_bool();


	bool* buf_key = new bool[56];

	for (int j = 0; j < 56; j++)
	{
		num_at_block_with_IP = IP_for_key[j] - 1;
		num_at_block = j;

		buf_key[num_at_block] = Key_[num_at_block_with_IP];
	}


	for (int i = 0; i < 56; i++)
	{
		Key_[i] = buf_key[i];

	}
	delete[] buf_key;




	for (int i = 0; i < 28; i++)
	{
		C[0][i] = Key_[i];
		D[0][i] = Key_[i + 28];
	}


	for (int i = 1; i < 3; i++)
	{

		for (int j = 0; j < 28; j++)
		{
			if ((j + 1 > 27)) C[i][j] = C[i - 1][0];
			C[i][j] = C[i - 1][j + 1];
		}

		for (int j = 0; j < 28; j++)
		{
			if ((j + 1 > 27)) D[i][j] = D[i - 1][0];
			D[i][j] = D[i - 1][j + 1];
		}
	}


	for (int i = 3; i < 9; i++)
	{
		for (int j = 0; j < 28; j++)
		{
			if ((j + 2 > 27)) C[i][j] = C[i - 1][27 - j - 2];
			C[i][j] = C[i - 1][j + 2];
		}

		for (int j = 0; j < 28; j++)
		{
			if ((j + 2 > 27)) D[i][j] = D[i - 1][27 - j - 2];
			D[i][j] = D[i - 1][j + 2];
		}
	}


	for (int j = 0; j < 28; j++)
	{
		if ((j + 1 > 27)) C[9][j] = C[8][0];
		C[9][j] = C[8][j + 1];
	}

	for (int j = 0; j < 28; j++)
	{
		if ((j + 1 > 27)) D[9][j] = D[8][0];
		D[9][j] = D[8][j + 1];
	}


	for (int i = 10; i < 16; i++)
	{
		for (int j = 0; j < 28; j++)
		{
			if ((j + 2 > 27)) C[i][j] = C[i - 1][27 - j - 2];
			C[i][j] = C[i - 1][j + 2];
		}

		for (int j = 0; j < 28; j++)
		{
			if ((j + 2 > 27)) D[i][j] = D[i - 1][27 - j - 2];
			D[i][j] = D[i - 1][j + 2];
		}
	}


	for (int j = 0; j < 28; j++)
	{
		if ((j + 1 > 27)) C[16][j] = C[15][0];
		C[16][j] = C[15][j + 1];
	}

	for (int j = 0; j < 28; j++)
	{
		if ((j + 1 > 27)) D[16][j] = D[15][0];
		D[16][j] = D[15][j + 1];
	}


	for (int i = 0; i < 16; i++)
	{
		bool* buf_ = new bool[56];
		for (int k = 0; k < 56; k++)
		{
			if (k < 28) buf_[k] = C[i + 1][k];
			else buf_[k] = D[i + 1][k - 28];
		}

		for (int k = 0; k < 48; k++)
		{
			num_at_block_with_IP = roker_key[k] - 1;
			num_at_block = k;

			keys_48bit[i][num_at_block] = buf_[num_at_block_with_IP];
		}
		delete[]buf_;
	}




	T.clear();
	T.resize(Text.GetLength() * 8);

	for (int i = 0; i < text.size() / 64; i++)
	{
		bool* L = new bool[32];
		bool* R = new bool[32];


		for (int k = 0; k < 32; k++)
		{
			L[k] = text[i * 64 + k];
			R[k] = text[i * 64 + 32 + k];
		}


		for (int ii = 1; ii < 17; ii++)
		{
			bool* E = new bool[48];
			for (int k = 0; k < 48; k++)
			{
				num_at_block_with_IP = expansion[k] - 1;
				num_at_block = k;
				E[num_at_block] = R[num_at_block_with_IP];
			}

			for (int k = 0; k < 48; k++)
			{
				E[k] = E[k] ^ keys_48bit[ii - 1][k];
			}

			bool** B = new bool* [8];
			for (int k = 0; k < 8; k++)
				B[k] = new bool[6];

			for (int k = 0; k < 8; k++)
			{
				for (int j = 0; j < 6; j++)
				{
					B[k][j] = E[k * 6 + j];
				}
			}

			delete[]E;

			bool** BB = new bool* [8];
			for (int k = 0; k < 8; k++)
				BB[k] = new bool[4];


			for (int k = 0; k < 8; k++)
			{
				int a, b, c;

				a = B[k][5] * 1 + B[k][0] * 2;
				b = B[k][1] * 2 * 2 * 2 + B[k][2] * 2 * 2 + B[k][3] * 2 + B[k][4] * 1;
				c = Sj[k][a * 16 + b];

				for (int j = 0; j < 4; j++)
				{
					BB[k][3 - j] = c % 2;
					c = c / 2;
				}
			}

			for (int k = 0; k < 8; k++) delete[] B[k];
			delete[] B;

			bool* f = new bool[32];
			bool* buf = new bool[32];
			for (int k = 0; k < 8; k++)
			{
				for (int j = 0; j < 4; j++)
				{
					buf[k * 4 + j] = BB[k][j];
				}
			}

			for (int k = 0; k < 8; k++)
				delete[] BB[k];
			delete[] BB;

			for (int k = 0; k < 32; k++)
			{
				num_at_block_with_IP = P[k] - 1;
				num_at_block = k;
				f[num_at_block] = buf[num_at_block_with_IP];
			}
			delete[] buf;


			bool* L_past = new bool[32];
			for (int k = 0; k < 32; k++)
			{
				L_past[k] = L[k];
				L[k] = R[k];
			}

			for (int k = 0; k < 32; k++)
			{
				R[k] = L_past[k] ^ f[k];
			}
			delete[] f;
			delete[]L_past;
		}




		bool* T_buf = new bool[64];
		for (int k = 0; k < 64; k++)
		{
			if (k < 32) T_buf[k] = L[k];
			else T_buf[k] = R[k - 32];
		}

		for (int k = 0; k < 64; k++)
		{
			num_at_block_with_IP = IP_1[k] - 1;
			num_at_block = k;
			T[i * 64 + num_at_block] = T_buf[num_at_block_with_IP];
		}

		delete[] T_buf;
		delete[]L;
		delete[] R;


	}

	int* cipher = new int[T.size() / 8];
	for (int k = 0; k < T.size() / 8; k++)
	{

		cipher[k] = (int)(T[7 + 8 * k] * 1 + T[6 + 8 * k] * 2 + T[5 + 8 * k] * 2 * 2 + T[4 + 8 * k] * 2 * 2 * 2 + T[3 + 8 * k] * 2 * 2 * 2 * 2 + T[2 + 8 * k] * 2 * 2 * 2 * 2 * 2 + T[1 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 + T[0 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 * 2);
	}

	for (int k = 0; k < T.size() / 8; k++)
	{

		Encr_txt.AppendChar(cipher[k]);
	}
	delete[] cipher;
	UpdateData(FALSE);
}


void CDESDlg::OnBnClickedButtonDecrypt()//Расшифровать
{
	// TODO: добавьте свой код обработчика уведомлений
	Decr_txt.Empty();



	const int Sj[8][64] = {
		{ 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
		0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
		4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
		15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 },

		{ 15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
		3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
		0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
		13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 },

		{ 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
		13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
		13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
		1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 },

		{ 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
		13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
		10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
		3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 },

		{ 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
		14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
		4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
		11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 },

		{ 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
		10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
		9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 1, 6,
		4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 1 },

		{ 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
		13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
		1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
		6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 },

		{ 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
		1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
		7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
		2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 }
	};

	const int P[] =
	{
		16, 7, 20, 21, 29, 12, 28, 17,
		1, 15, 23, 26, 5, 18, 31, 10,
		2, 8, 24, 14, 32, 27, 3, 9,
		19, 13, 30, 6, 22, 11, 4, 25
	};

	const int IP[] = { 58, 50, 42, 34, 26, 18, 10, 2,
		60, 52, 44, 36, 28, 20, 12, 4,
		62, 54, 46, 38, 30, 22, 14, 6,
		64, 56, 48, 40, 32, 24, 16, 8,
		57, 49, 41, 33, 25, 17, 9, 1,
		59, 51, 43, 35, 27, 19, 11, 3,
		61, 53, 45, 37, 29, 21, 13, 5,
		63, 55, 47, 39, 31, 23, 15, 7 };

	const int IP_1[] = { 40, 8, 48, 16, 56, 24, 64, 32,
		39, 7, 47, 15, 55, 23, 63, 31,
		38, 6, 46, 14, 54, 22, 62, 30,
		37, 5, 45, 13, 53, 21, 61, 29,
		36, 4, 44, 12, 52, 20, 60, 28,
		35, 3, 43, 11, 51, 19, 59, 27,
		34, 2, 42, 10, 50, 18, 58, 26,
		33, 1, 41, 9, 49, 17, 57, 25 };


	const int IP_for_key[] = { 57, 49, 41, 33, 25, 17, 9, 1,
		58, 50, 42, 34, 26, 18, 10, 2,
		59, 51, 43, 35, 27, 19, 11, 3,
		60, 52, 44, 36, 63, 55, 47, 39,
		31, 23, 15, 7, 62, 54, 46, 38,
		30, 22, 14, 6, 61, 53, 45, 37,
		29, 21, 13, 5, 28, 20, 12, 4 };

	const int roker_key[] = { 14, 17, 11, 24, 1, 5,
		3, 28, 15, 6, 21, 10,
		23, 19, 12, 4, 26, 8,
		16, 7, 27, 20, 13, 2,
		41, 52, 31, 37, 47, 55,
		30, 40, 51, 45, 33, 48,
		44, 49, 39, 56, 34, 53,
		46, 42, 50, 36, 29, 32 };

	static int expansion[] = { 32, 1, 2, 3, 4, 5, 4, 5,
		6, 7, 8, 9, 8, 9, 10, 11,
		12, 13, 12, 13, 14, 15, 16, 17,
		16, 17, 18, 19, 20, 21, 20, 21,
		22, 23, 24, 25, 24, 25, 26, 27,
		28, 29, 28, 29, 30, 31, 32, 1 };

	str_encrypt_to_bool();
	decoder_text.clear();
	decoder_text.resize(Encr_txt.GetLength() * 8);
	int number_at_block_with_IP, number_at_block;




	for (int i = 0; i < shifr_text.size() / 64; i++)
	{
		bool* L = new bool[32];
		bool* R = new bool[32];
		bool* T_buf = new bool[64];

		for (int k = 0; k < 64; k++)
		{
			number_at_block_with_IP = IP_1[k] - 1;
			number_at_block = k + i * 64;
			T_buf[number_at_block_with_IP] = shifr_text[number_at_block];
		}

		for (int k = 0; k < 64; k++)
		{
			if (k < 32) L[k] = T_buf[k];
			else R[k - 32] = T_buf[k];
		}
		delete[] T_buf;


		for (int ii = 16; ii > 0; ii--)
		{
			bool* E = new bool[48];
			for (int k = 0; k < 48; k++)
			{
				number_at_block_with_IP = expansion[k] - 1;
				number_at_block = k;
				E[number_at_block] = L[number_at_block_with_IP];
			}

			for (int k = 0; k < 48; k++)
			{
				E[k] = E[k] ^ keys_48bit[ii - 1][k];
			}

			bool** B = new bool* [8];
			for (int k = 0; k < 8; k++) B[k] = new bool[6];

			for (int k = 0; k < 8; k++)
			{
				for (int j = 0; j < 6; j++)
				{
					B[k][j] = E[k * 6 + j];
				}
			}

			delete[]E;

			bool** BB = new bool* [8];
			for (int k = 0; k < 8; k++) BB[k] = new bool[4];


			for (int k = 0; k < 8; k++)
			{
				int a, b, c;
				a = B[k][5] * 1 + B[k][0] * 2;
				b = B[k][1] * 2 * 2 * 2 + B[k][2] * 2 * 2 + B[k][3] * 2 + B[k][4] * 1;
				c = Sj[k][a * 16 + b];

				for (int j = 0; j < 4; j++)
				{
					BB[k][3 - j] = c % 2;
					c = c / 2;
				}
			}

			for (int k = 0; k < 8; k++) delete[] B[k];
			delete[] B;

			bool* f = new bool[32];
			bool* buf = new bool[32];
			for (int k = 0; k < 8; k++)
			{
				for (int j = 0; j < 4; j++)
				{
					buf[k * 4 + j] = BB[k][j];
				}
			}

			for (int k = 0; k < 8; k++) delete[] BB[k];
			delete[] BB;

			for (int k = 0; k < 32; k++)
			{
				number_at_block_with_IP = P[k] - 1;
				number_at_block = k;
				f[number_at_block] = buf[number_at_block_with_IP];
			}
			delete[] buf;


			bool* R_past = new bool[32];
			for (int k = 0; k < 32; k++)
			{
				R_past[k] = R[k];
				R[k] = L[k];
			}

			for (int k = 0; k < 32; k++)
			{
				L[k] = R_past[k] ^ f[k];
			}
			delete[] f;
			delete[]R_past;
		}

		for (int k = 0; k < 64; k++)
		{
			if (k < 32) decoder_text[i * 64 + k] = L[k];
			else decoder_text[i * 64 + k] = R[k - 32];
		}

		delete[] R;
		delete[]L;

	}



	bool* buffer = new bool[Encr_txt.GetLength() * 8];
	for (int i = 0; i < Encr_txt.GetLength() / 8; i++)
	{
		for (int j = 0; j < 64; j++)
		{
			number_at_block_with_IP = i * 64 + IP[j] - 1;
			number_at_block = i * 64 + j;
			buffer[number_at_block_with_IP] = decoder_text[number_at_block];
		}
	}

	for (int i = 0; i < Text.GetLength() * 8; i++)
	{
		decoder_text[i] = buffer[i];
	}
	delete[]buffer;

	int* decipher = new int[shifr_text.size() / 8];
	for (int k = 0; k < T.size() / 8; k++)
	{
		decipher[k] = (int)(decoder_text[7 + 8 * k] * 1 + decoder_text[6 + 8 * k] * 2 + decoder_text[5 + 8 * k] * 2 * 2 + decoder_text[4 + 8 * k] * 2 * 2 * 2 + decoder_text[3 + 8 * k] * 2 * 2 * 2 * 2 + decoder_text[2 + 8 * k] * 2 * 2 * 2 * 2 * 2 + decoder_text[1 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 + decoder_text[0 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 * 2);
	}

	for (int k = 0; k < decoder_text.size() / 8; k++)
	{
		Decr_txt.AppendChar(decipher[k]);
	}
	delete[] decipher;
	UpdateData(FALSE);
}

