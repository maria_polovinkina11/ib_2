﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется DES.rc
//
#define IDD_DES_DIALOG                  102
#define IDR_MAINFRAME                   128
#define IDC_ishod_txt                   1000
#define IDC_decrypt_txt                 1001
#define IDC__Encrypt_txt                1002
#define IDC_BUTTON_GenKey               1003
#define IDC_key                         1004
#define IDC_BUTTON_Decrypt              1005
#define IDC_BUTTON_Encrypt              1006
#define IDC_BUTTON_Start                1007
#define IDC_BUTTON_GenKey2              1008
#define IDC_BUTTON_Load                 1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
