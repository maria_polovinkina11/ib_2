﻿
// DESDlg.h: файл заголовка
//

#pragma once
#include <vector>

using namespace std;
// Диалоговое окно CDESDlg
class CDESDlg : public CDialogEx
{
// Создание
public:
	CDESDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DES_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonEncrypt();
	afx_msg void OnBnClickedButtonDecrypt();
	afx_msg void OnBnClickedButtonGenkey();
	afx_msg void OnBnClickedButtonLoad();
	CString Text;
	CString Encr_txt;
	CString Decr_txt;
	CString Key;



	void txt_to_bool();
	void str_encrypt_to_bool();
	void key_to_bool();
	//void create_keys();

	vector <bool> text;
	vector <bool> shifr_text;
	vector <bool> Key_;
	vector <bool> T;
	vector <bool> decoder_text;
	//bool Var;

	bool keys_48bit[16][48];
	bool C[17][28];
	bool D[17][28];

};
